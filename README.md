# The Dilemmnist  
## About the Game  

<div align="center">
    <img alt="[Animated image of the main menu]" src="https://dl.michaelpmiddleton.com/ludum-dare/43/showcase.gif"/>
</div>
<br>
The Dilemmnist is a game the plays on the idea of the "Trolley Problem" moral exercise in which the player must choose which track of people to save from incoming subways. Players may choose to play either a "Purely Utilitarian" or what I call a "Forward-Minded" playthrough.  
<br>  
<br>  

## Developer Notes
I made a full post-mortem on this project. To read it, [click here](https://ldjam.com/events/ludum-dare/43/the-dilemmnist/the-dilemmnist-a-post-mortem).  
<br>  
<br>  

## Play the Game  
If you are feel so inclined, feel free to try out the game! Links to the binaries for the supported platforms are below:  
* [Windows](https://dl.michaelpmiddleton.com/ludum-dare/43/submission_Windows.zip)
* [OSX / MacOS](https://dl.michaelpmiddleton.com/ludum-dare/43/submission_MacOS.zip) 