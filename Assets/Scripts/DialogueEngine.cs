/*
	FILE:		DialogueEngine.cs
	AUHTOR:		mmiddleton
	DATE:		2 DEC 2018

	DESCRIPTION:

	LEGAL:
	Copyright © 2018 Michael P. Middleton
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueEngine : MonoBehaviour {
    private string[] m_CannedResponses = {
        "Interesting.",
        "I wonder what brought you to this decision...",
        "Oh my!",
        "Unfortunately, we must continue.",
        "Sally forth!",
        "Moving on."
    };

    public void Intro (int timesThrough) {
        switch (timesThrough) {
            case 0:
                UIManager.Instance.SetDialogueText (
                    new string[]{
                        "Welcome Dilemmnist! We were worried you wouldn't make it.",
                        "Since this is your area of expertise, we decided to wait for you before we made any decisions.",
                        "Are you ready?",
                        "Quiet huh? That's okay, most are the first time.",
                        "Good luck."
                    },
                    5,
                    new string[]{
                        "[ Next ]",
                        "[ Next ]",
                        "...",
                        "What's goi-",
                        "[ Play Game ]"
                    }
                );
                break;

            case 1:
                UIManager.Instance.SetDialogueText (
                    new string[]{
                        "Dilemmnist, you've returned! How curious.",
                        "One would think that going through this ordeal once would be enough.",
                        "Well, good luck once again."
                    },
                    3,
                    new string[]{
                        "[ Next ]",
                        "[ Next ]",
                        "[ Play Game ]"
                    }
                );
                break;

            case 2:
                UIManager.Instance.SetDialogueText (
                    new string[]{
                        "You're here to try yet again?",
                        "Careful though!",
                        "If you keep this up, people will think you like this!",
                        "Well, good luck once again."
                    },
                    4,
                    new string[]{
                        "[ Next ]",
                        "[ Next ]",
                        "[ Play Game ]"
                    }
                );
                break;

            case 3:
                UIManager.Instance.SetDialogueText (
                    new string[]{
                        "*You've received a note from THE DEVELOPER*",
                        "\"Hey there! So you've played this a few times through. For that I'm so humbled! --",
                        "-- Thank you so much for giving my game a thorough play-through. You're Awesome!--",
                        "You won't hear directly from me again. But good luck out there! -mmiddleton\""
                    },
                    4,
                    new string[]{
                        "[ Read Message ]",
                        "[ ... ]",
                        "[ I'm Awesome ]",
                        "[ Play Game ]"
                    }
                );
                break;

            default:
                UIManager.Instance.SetDialogueText (
                    new string[]{
                        "... good luck."
                    },
                    1,
                    new string[]{
                        "[ Play Game ]"
                    }
                );
                break;
        }

        UIManager.Instance.UpdateDialogueText (true);
        // TODO
    }
    public void CommentaryOnRound (CharacterObject[][] tracks, bool trackOneSaved, bool trackTwoSaved) {
        GameManager.Instance.GameState = GameManager.GameStates.CUTSCENE;
        UIManager.Instance.ShowDialogue ();
        string[] commentary = new string[3];
        int commentaryLength = 0;
        
        if (!trackOneSaved && !trackTwoSaved) {
            commentary[commentaryLength] = "Dilemmnist! This is not the time to freeze. The choices are hard, but sacrifices must be made!";
            commentaryLength++;
        }

        else {
            // TODO: Add more flavour to the text
            for (int c = 0; c < 2; c++) {
                foreach (CharacterObject victim in tracks[c]) {
                    if (victim.Name == "")  continue;

                    // Saved dialogue:
                    if ((c == 0 && trackOneSaved) || (c == 1 && trackTwoSaved)) {
                        switch (victim.Occupation) {
                            case "Your Child":
                                commentary[commentaryLength] = "Ah... So you chose to save your child. Beneficially selfish, but at what cost?";
                                commentaryLength++;
                                break;

                            case "CEO":
                                commentary[commentaryLength] = "A CEO's deep pockets could definitely prove useful...";
                                commentaryLength++;
                                break;

                            case "?????":
                                commentary[commentaryLength] = "Hm... That person looks kinda sick.";
                                commentaryLength++;
                                break;

                            case "Elder of the Internet":
                                commentary[commentaryLength] = "The Elders of the Internet now know who you are!";
                                commentaryLength++;
                                break;

                            default:
                                break;
                        }
                    }

                    // Sacrificed dialogue:
                    else {
                        switch (victim.Occupation) {
                            case "Your Child":
                                commentary[commentaryLength] = "You would sacrifice a child?";
                                commentaryLength++;
                                break;

                            case "That Guy":
                                commentary[commentaryLength] = "You just killed that guy!";
                                commentaryLength++;
                                break;

                            case "Dilemmnist":
                                commentary[commentaryLength] = "You died to save those the other(s). You will be remembered.";
                                commentaryLength++;
                                break;

                            default:
                                break;
                        }
                    }
                }
            }
        }

        // If we haven't had any comments yet, add a canned response.
        if (commentaryLength == 0) {
            commentary[commentaryLength] = m_CannedResponses[Random.Range (0, m_CannedResponses.Length)];
            commentaryLength++;
        }

        UIManager.Instance.SetDialogueText (commentary, commentaryLength);
        UIManager.Instance.UpdateDialogueText ();
    }
}