/*
	FILE:		GameManager.cs
	AUHTOR:		mmiddleton
	DATE:		1 DEC 2018

	DESCRIPTION:
	This file comprises the main runtime processes and links to the other managers of the game.

	LEGAL:
	Copyright © 2018 Michael P. Middleton
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameManager : MonoBehaviour {
    public static GameManager Instance;
    public enum GameStates { PLAYING = 0, CUTSCENE = 1 };
    public GameStates GameState;
    public ConflictsManager ConflictManager;
    public DialogueEngine Dialogue;
    public CharacterObject[] Saved;
    public string[] Sacrificed;
    public float DecisionTimerMax;  // Set in Awake ()
    public int NumberSaved { get { return m_NumberSaved; } }
    public int NumberSacrificed { get { return m_NumberSacrificed; } }
    public bool WasLastRound { get { return m_Round >= m_NumberOfRounds; } }

    [SerializeField]
    private CharacterObject m_CharacterObjectPrefab;
	private int m_NumberSaved = 0;
	private int m_NumberSacrificed = 0;
    [SerializeField]
    private int m_Round = 0;
    private int m_NumberOfRounds;   // set in Awake ()
    private float m_DecisionTimer = 0.0f;
    private int m_TimesPLayed = 0;



    public void Awake () {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad (gameObject);

            GameState = GameStates.CUTSCENE;
            DecisionTimerMax = 15.0f;
            m_DecisionTimer = DecisionTimerMax;
            m_NumberOfRounds = 5;
            Saved = new CharacterObject[3 * m_NumberOfRounds];
            Sacrificed = new string[6 * m_NumberOfRounds];

            CharacterObject.LoadCharacterData ();
            ConflictManager.LoadConflicts ();
            
            ConflictManager.GenerateNewTrackConflict ();
        }

        else {
            if (Instance != this)
                Destroy (gameObject);
        }
    }
    public void Update () {
        if (GameState == GameStates.PLAYING) {
            if (m_Round < m_NumberOfRounds) {
                if (m_DecisionTimer > 0) {
                    m_DecisionTimer -= Time.deltaTime;
                    UIManager.Instance.UpdateTimerText (System.Convert.ToString ((int) m_DecisionTimer + 1));
                }
                else {
                    Dialogue.CommentaryOnRound (ConflictManager.GetTracks (), false, false);                    
                    m_NumberSacrificed += ConflictManager.KillTrack (1);
                    m_NumberSacrificed += ConflictManager.KillTrack (2);
                    NextRound ();
                }
            }
            else {
                GameState = GameStates.CUTSCENE;
                // TODO: Dialogue Engine interaction
            }
        }
        
        if (Input.GetKeyUp ("escape")) {
            Debug.Log ("Game is closing. Thanks for playing!");
            Application.Quit ();
        }
    }


    public void Reset () {
        for (int c = 0; c < m_NumberSaved; c++)
            Destroy (Saved[c]);

        Saved = new CharacterObject[3 * m_NumberOfRounds];

        for (int c = 0; c < m_NumberSacrificed; c++)
            Sacrificed[c] = "";

        m_NumberSacrificed = m_Round = m_NumberSaved = 0;
        ConflictManager.Reset ();
        UIManager.Instance.SetNewsReelText (ConflictManager.NewsReelText);
    }


    public void ChooseTrack (int choice) {
        Dialogue.CommentaryOnRound (
            ConflictManager.GetTracks (),
            choice == 1 ? true : false,
            choice == 2 ? true : false
        );   

        switch (choice) {
            case 1:
                m_NumberSaved += ConflictManager.SaveTrack (1);
                m_NumberSacrificed += ConflictManager.KillTrack (2);
                break;

            case 2:
                m_NumberSaved += ConflictManager.SaveTrack (2);
                m_NumberSacrificed += ConflictManager.KillTrack (1);
                break;

            default:
                Debug.LogError ("This should never happen!");
                break;
        }

        if (m_Round + 1 <= m_NumberOfRounds) {
            NextRound ();
        }

        else {
            GameState = GameStates.CUTSCENE;
            // TODO: Dialogue engine interation [end game]
        }
    }

    public void NextRound () {
        m_DecisionTimer = DecisionTimerMax;
        ConflictManager.GenerateNewTrackConflict ();
        m_Round++;

        UIManager.Instance.SetNewsReelText (ConflictManager.NewsReelText);
    }
    public void NewCharacterObject (int offset, CharacterObject data) {
        // Create new CharacterObject
        CharacterObject newCharacterObject = Instantiate (m_CharacterObjectPrefab);
        newCharacterObject.Name = data.Name;
        newCharacterObject.Occupation = data.Occupation;
        newCharacterObject.ChildName = data.ChildName;
        newCharacterObject.FlavourText = data.FlavourText;
        newCharacterObject.Plea = data.Plea;

        // Hide within the GameManager and add it to the list of saved people
        Saved[NumberSaved + offset] = newCharacterObject;
        newCharacterObject.transform.SetParent (transform);
    }

    public void ProcessEndGame () {
        UIManager.Instance.GameOverSaved.text = ("You saved " + NumberSaved + " people.");
        UIManager.Instance.GameOverSacrificed.text = ("You sacrificed " + NumberSacrificed + " people.");
        
        ConflictManager.ResolveAll (Saved);
        UIManager.Instance.GameOverSurvivalStatus.text = ConflictManager.GetRelevantResolutionText ();

        UIManager.Instance.ShowGameOverScreen ();
    }

    public void StartGame () {
        Reset ();
        UIManager.Instance.CloseMenu ();
        UIManager.Instance.ShowDialogue ();
        Dialogue.Intro (m_TimesPLayed);
        m_TimesPLayed++;
    }
}