/*
	FILE:		AnimationManager.cs
	AUHTOR:		mmiddleton
	DATE:		2 DEC 2018

	DESCRIPTION:

	LEGAL:
	Copyright © 2018 Michael P. Middleton
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class AnimationManager : MonoBehaviour {
    public static AnimationManager Instance;
    [SerializeField]
    private Animator[] m_Pleas;

    [SerializeField]
    private Animator m_NewsReel;

    private float m_PleaLength;
    private float m_PleaTimer;
    private int m_ActivePlea;

    public void Awake () {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad (gameObject);
        }

        else 
            if (Instance != this) {
                Destroy (gameObject);
                return;
            }

        m_PleaLength = 3.0f;
        m_PleaTimer = 0;   
    }
    public void Update () {
        if (GameManager.Instance.GameState == GameManager.GameStates.PLAYING) {
            if (m_PleaTimer < 0) {
                m_ActivePlea = Random.Range (0, 6);
                m_PleaTimer = m_PleaLength;
                ActivatePlea ();
            }

            else {
                m_PleaTimer -= Time.deltaTime;
            }
        }
    }

    public void AnimateNewsReel () {
        m_NewsReel.SetTrigger ("Content Updated");
    }
    public void ResetNewsReel () {
        m_NewsReel.ResetTrigger ("Content Updated");
    }
    
    public void ActivatePlea () {
        for (int c = 0; c < 6; c++) {
            m_Pleas[c].SetBool ("IsActivated", false);
        }

        if (m_Pleas[m_ActivePlea].transform.parent.GetComponent<CharacterObject> ().Name != null) {
            m_Pleas[m_ActivePlea].SetBool ("IsActivated", true);
        }
    }
}