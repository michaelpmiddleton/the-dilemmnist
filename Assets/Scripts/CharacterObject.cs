/*
	FILE:		CharacterObject.cs
	AUHTOR:		mmiddleton
	DATE:		1 DEC 2018

	DESCRIPTION:

	LEGAL:
	Copyright © 2018 Michael P. Middleton
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterObject : MonoBehaviour {
	public string Name;
	public string Occupation;
	public string FlavourText;
	public string Plea;
	public string ChildName;

	public static bool CharacterDataEmpty = true;
	private static CharacterObject[] m_CharacterData;

	public static void LoadCharacterData () {
		if (CharacterDataEmpty) {
			TextAsset rawCharacterData = Resources.Load<TextAsset> ("CharacterAttributes");
			string[] splitCharacterData = rawCharacterData.text.Split (new char[] {'\n'});
			m_CharacterData = new CharacterObject[splitCharacterData.Length-1];

			// Start at 1 because the first line is the headers
			for (int c = 1; c < splitCharacterData.Length; c++) {
				string[] singleRowData = splitCharacterData[c].Split (new char[] {','});

				m_CharacterData[c-1] = new CharacterObject ();
				m_CharacterData[c-1].Name = singleRowData[0];
				m_CharacterData[c-1].Occupation = singleRowData[1];
				m_CharacterData[c-1].ChildName = singleRowData[2];
				m_CharacterData[c-1].Plea = singleRowData[3];
				// m_CharacterData[c-1].FlavourText = singleRowData[4];
			}
			
			CharacterDataEmpty = false;
		}
		else
			Debug.Log ("Character data has already been loaded!");

	}


	public void PopulateData () {
		int newVictimIndex;
		bool seen;

		do {
			seen = false;
			newVictimIndex = (int)(Random.Range (0, m_CharacterData.Length));
			// Check current track
			foreach (string name in GameManager.Instance.ConflictManager.OnTheTrack)
				if (name == m_CharacterData[newVictimIndex].Name) {
					seen = true;
					break;
				}

			// Check killed list
			foreach (string name in GameManager.Instance.Sacrificed)
				if (name == m_CharacterData[newVictimIndex].Name) {
					seen = true;
					break;
				}
			
			// Check saved list
			for (int c = 0; c < GameManager.Instance.NumberSaved; c++)
				if (GameManager.Instance.Saved[c].Name == m_CharacterData[newVictimIndex].Name) {
					seen = true;
					break;
				}
			
		} while (seen);
		
		Name = m_CharacterData[newVictimIndex].Name;
		Occupation = m_CharacterData[newVictimIndex].Occupation;
		ChildName = m_CharacterData[newVictimIndex].ChildName;
		FlavourText = m_CharacterData[newVictimIndex].FlavourText;
		Plea = m_CharacterData[newVictimIndex].Plea;
	}

	public void ClearData () {
		Name = Occupation = FlavourText = Plea = ChildName = null;
	}

	public bool HasChild () {
		return ChildName != "";
	}

	private void OnMouseEnter() {
		if (Name != "" && GameManager.Instance.GameState == GameManager.GameStates.PLAYING) {
			UIManager.Instance.UpdateToolTip (this);
			UIManager.Instance.ShowToolTip ();
		}
	}

	private void OnMouseExit() {
		UIManager.Instance.HideToolTip ();	
	}
}