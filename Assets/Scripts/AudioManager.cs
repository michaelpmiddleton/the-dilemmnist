/*
	FILE:		AudioManager.cs
	AUHTOR:		mmiddleton
	DATE:		3 DEC 2018

	DESCRIPTION:

	LEGAL:
	Copyright © 2018 Michael P. Middleton
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour {
    public static AudioManager Instance;
    public AudioSource Music;
    [SerializeField]
    private AudioClip m_Music_Menu;
    [SerializeField]
    private AudioClip m_Music_InGame;
    
    public enum Songs {MENU = 0, IN_GAME = 1};
    
    private void Awake () {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad (gameObject);
        }

        else {
            if (Instance != this) {
                Destroy (gameObject);
                return;
            }
        }

        
    }

    private void Start () {
        PlaySong (Songs.MENU);
    }   

    public void PlaySong (Songs song) {
        Music.Stop ();

        switch (song) {
            case Songs.MENU:
                Music.clip = m_Music_Menu;
                break;

            case Songs.IN_GAME:
                Music.clip = m_Music_InGame;
                break;

            default:
                break;
        }

        Music.Play ();
    }
}