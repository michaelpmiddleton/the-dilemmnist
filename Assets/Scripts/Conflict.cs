/*
	FILE:		Conflict.cs
	AUHTOR:		mmiddleton
	DATE:		2 DEC 2018

	DESCRIPTION:

	LEGAL:
	Copyright © 2018 Michael P. Middleton
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Conflict {
    public int ID;
    public string Name;
    public bool Solved;
    public int SolutionCount;
    public string Resolution;
    public string NewsReelText;

    private string m_Cause;
    private string[] m_Solutions;
    private bool m_CauseIsAlive;

    public Conflict (int id, string rawConflictData) {
        ID = id;
        Solved = false;
        string[] parsedConflictData = rawConflictData.Split (new char[] {','});

        Name = parsedConflictData[0];
        m_Cause = parsedConflictData[1];
        m_CauseIsAlive = System.Convert.ToInt32 (parsedConflictData[2]) == 1 ? true : false;
        NewsReelText = System.Convert.ToInt32 (parsedConflictData[3]) == 1 ? parsedConflictData[4] : null;

        SolutionCount = System.Convert.ToInt32 (parsedConflictData[5]);
        
        if (SolutionCount > 0) {
            m_Solutions = new string[SolutionCount];
            for (int c = 0; c < m_Solutions.Length; c++)
                m_Solutions[c] = parsedConflictData[6 + c];
        }
    }
    
    public bool CausesConflict (string occupation, bool isAlive) {
        return (m_Cause == occupation) && (isAlive == m_CauseIsAlive);
    }

    public bool ResolvesConflict (string[] saved) {
        if (SolutionCount == 0) return false;

        foreach (string solutionPiece in m_Solutions) {
            int found = System.Array.IndexOf (saved, solutionPiece);
            if (found == -1) return false;
        }

        return true;
    }
}