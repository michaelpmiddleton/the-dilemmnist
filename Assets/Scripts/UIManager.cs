/*
	FILE:		UIManager.cs
	AUHTOR:		mmiddleton
	DATE:		1 DEC 2018

	DESCRIPTION:

	LEGAL:
	Copyright © 2018 Michael P. Middleton
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour {
    public static UIManager Instance;
    public TextMeshProUGUI TimerText;
    public GameObject ToolTip;
    public TextMeshProUGUI TT_Name;
    public TextMeshProUGUI TT_Occupation;
    public TextMeshProUGUI DialogueWindowText;
    public TextMeshProUGUI GameOverSaved;
    public TextMeshProUGUI GameOverSacrificed;
    public TextMeshProUGUI GameOverSurvivalStatus;
    public TextMeshProUGUI NewsReelText;
    public Button SaveTrackOne;
    public Button SaveTrackTwo;
    public TextMeshProUGUI DialogueWindowButton;


    [SerializeField]
    private GameObject m_DialogueWindow;
    [SerializeField]
    private GameObject m_PlayerChoiceWindow;
    [SerializeField]
    private GameObject m_MenuScreen;
    [SerializeField]
    private GameObject m_GameScreen;
    [SerializeField]
    private GameObject m_GameOverScreen;
    [SerializeField]
    private TextMeshProUGUI[] m_VictimPleas;
    
    private string[] m_DialogueButtonContent;
    private string[] m_DialogueContent;
    private int m_DialogueContentIndex;
    private int m_DialogueButtonIndex;
    private int m_DialogueContentMax;


    // Unity Methods:
    private void Awake () {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad (gameObject);
        }

        else {
            if (Instance != this)
                Destroy (gameObject);
        }
    }
    private void Start () {
        ReturnToMainMenu ();
    }



    // User-Defined Methods:
    public void UpdateTimerText (string value) {
        TimerText.text = value;
    }    

    public void UpdateToolTip (CharacterObject data) {
        TT_Name.text = data.Name;
        TT_Occupation.text = data.Occupation;
    }

    public void ShowToolTip () {
        ToolTip.SetActive (true);
    }

    public void HideToolTip () {
        ToolTip.SetActive (false);
    }

    public void ShowDialogue () {
        m_DialogueWindow.SetActive (true);
        m_PlayerChoiceWindow.SetActive (false); 
    }

    public void HideDialogue () {
        m_DialogueWindow.SetActive (false);
        m_PlayerChoiceWindow.SetActive (true); 
    }

    public void ReturnToMainMenu () {
        m_MenuScreen.SetActive (true);
        m_GameScreen.SetActive (false);
        m_GameOverScreen.SetActive (false);
        AudioManager.Instance.PlaySong (AudioManager.Songs.MENU);
    }
    public void CloseMenu () {
        m_MenuScreen.SetActive (false);
        m_GameScreen.SetActive (true);
        AudioManager.Instance.PlaySong (AudioManager.Songs.IN_GAME);
    }
    public void ShowGameOverScreen () {
        m_MenuScreen.SetActive (false);
        m_GameScreen.SetActive (false);
        m_GameOverScreen.SetActive (true);
    }

    public void SetDialogueText (string[] comments, int ammountOfComments, string[] buttonContent = null) {
        GameManager.Instance.GameState = GameManager.GameStates.CUTSCENE;
        m_DialogueContentMax = ammountOfComments;
        m_DialogueButtonIndex = m_DialogueContentIndex = 0;
        m_DialogueContent = comments;
        m_DialogueButtonContent = buttonContent;
        DialogueWindowButton.gameObject.SetActive (true);
    }
    public void UpdateDialogueText (bool animated = false) {
        if (m_DialogueContentIndex < m_DialogueContentMax) {
            DialogueWindowText.text = m_DialogueContent[m_DialogueContentIndex];
            m_DialogueContentIndex++;

            if (m_DialogueButtonContent == null) 
                DialogueWindowButton.text = "[ Continue ]";

            else {
                DialogueWindowButton.text = m_DialogueButtonContent[m_DialogueButtonIndex];
                m_DialogueButtonIndex++;
            }
        }

        else {
            if (GameManager.Instance.WasLastRound) {
                GameManager.Instance.ProcessEndGame ();
                DialogueWindowButton.gameObject.SetActive (false);
            }

            else {
                HideDialogue ();
                GameManager.Instance.GameState = GameManager.GameStates.PLAYING;
                AnimationManager.Instance.AnimateNewsReel ();
                UIManager.Instance.SetNewsReelText (GameManager.Instance.ConflictManager.NewsReelText);
            }
        }
    }
    public void SetNewsReelText (string newData) {
        NewsReelText.text = newData;
    }
    public void SetPleaText (int index, string newPlea) {
        m_VictimPleas[index].text = newPlea;
    }
}