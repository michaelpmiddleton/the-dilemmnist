/*
	FILE:		ConflictsManager.cs
	AUHTOR:		mmiddleton
	DATE:		1 DEC 2018

	DESCRIPTION:

	LEGAL:
	Copyright © 2018 Michael P. Middleton
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConflictsManager : MonoBehaviour {

	public CharacterObject[] TrackOneVictims;
	public CharacterObject[] TrackTwoVictims;

	public Image[] TrackOneSprites;
	public Image[] TrackTwoSprites;
	
	public Sprite[] PossibleSprites;
	public string[] OnTheTrack = new string[6];
	public int numberOfSprites;
	public string NewsReelText;
	public string CauseOfDeath;
	public bool HasResolutions {get { return m_ResolvedConflicts.Count > 0; } }
	private Conflict[] m_PossibleConflicts;
	private ArrayList m_CurrentConflicts;
	private ArrayList m_ResolvedConflicts;
	private int m_TrackOneSize;
	private int m_TrackTwoSize;
	private string m_NewsRealContent;

	public void LoadConflicts() {
		m_CurrentConflicts = new ArrayList ();
		m_ResolvedConflicts = new ArrayList ();

		TextAsset rawConflictData = Resources.Load<TextAsset> ("ConflictList");
		string[] conflictDataInLines = rawConflictData.text.Split (new char[] {'\n'});
		m_PossibleConflicts = new Conflict[conflictDataInLines.Length - 1]; 
		
		// Set to 1 to skip the format line
		for (int c = 1; c < conflictDataInLines.Length; c++) {
			m_PossibleConflicts[c - 1] = new Conflict (c - 1, conflictDataInLines[c]);
		}
		Debug.Log ("Loaded in " + m_PossibleConflicts.Length + " conflicts from ConflictList.csv!");
	}

	public void GenerateNewTrackConflict () {
		OnTheTrack = new string[6];

		// Track One:
		m_TrackOneSize = System.Convert.ToInt32 (Random.Range (1, 4));
		for (int c = 0; c < 3; c++) {
			if (c <= m_TrackOneSize) {
				TrackOneVictims[c].PopulateData ();
				TrackOneSprites[c].enabled = true;
				TrackOneSprites[c].sprite = PossibleSprites[(int)(Random.Range (0, numberOfSprites))];
			}
			else {
				TrackOneSprites[c].enabled = false;
			}

			UIManager.Instance.SetPleaText (c, TrackOneVictims[c].Plea);
		}

		// Track Two:
		m_TrackTwoSize = System.Convert.ToInt32 (Random.Range (1, 4));
		for (int c = 0; c < 3; c++) {
			if (c < m_TrackTwoSize) {
				TrackTwoVictims[c].PopulateData ();
				TrackTwoSprites[c].enabled = true;
				TrackTwoSprites[c].sprite = PossibleSprites[(int)(Random.Range (0, numberOfSprites))];
			}
			else {
				TrackTwoSprites[c].enabled = false;
			}

			UIManager.Instance.SetPleaText (3 + c, TrackTwoVictims[c].Plea);
		}
	}

	public int SaveTrack (int index) {
		ProcessTrackConflicts (index, true);
		
		switch (index) {
			case 1:
				for (int c = 0; c < m_TrackOneSize; c++) {
					GameManager.Instance.NewCharacterObject (c, TrackOneVictims[c]);
					TrackOneVictims[c].ClearData ();
					TrackOneSprites[c].sprite = null;
				}
				return m_TrackOneSize;

			case 2:
				for (int c = 0; c < m_TrackTwoSize; c++) {
					GameManager.Instance.NewCharacterObject (c, TrackTwoVictims[c]);
					TrackTwoVictims[c].ClearData ();
					TrackTwoSprites[c].sprite = null;
				}
				return m_TrackTwoSize;
		
			default:
				return 0;
		}
	}

	public int KillTrack (int index) {
		ProcessTrackConflicts (index, false);
		switch (index) {
			case 1:
				for (int c = 0; c < m_TrackOneSize; c++) {
					GameManager.Instance.Sacrificed[GameManager.Instance.NumberSacrificed + c] = TrackOneVictims[c].Name;					
					TrackOneVictims[c].ClearData ();
				}
				return m_TrackOneSize; 

			case 2:
				for (int c = 0; c < m_TrackTwoSize; c++) {
					GameManager.Instance.Sacrificed[GameManager.Instance.NumberSacrificed + c] = TrackTwoVictims[c].Name;					
					TrackTwoVictims[c].ClearData ();
				}
				return m_TrackTwoSize;

			default:
				return 0;
		}
	}

	public void ProcessTrackConflicts (int track, bool isAlive) {
		foreach (Conflict potentialConflict in m_PossibleConflicts) {
			switch (track) {
				case 1:
					for (int c = 0; c < m_TrackOneSize; c++) {
						if (potentialConflict.CausesConflict (TrackOneVictims[c].Occupation, isAlive)) {
							m_CurrentConflicts.Add (potentialConflict);

							if (potentialConflict.NewsReelText != null) {
								NewsReelText += "BREAKING NEWS: " + (potentialConflict.NewsReelText + "  ");
							}
						}
					}
					break;

				case 2:
					for (int c = 0; c < m_TrackTwoSize; c++) {
						if (potentialConflict.CausesConflict (TrackTwoVictims[c].Occupation, isAlive)) {
							m_CurrentConflicts.Add (potentialConflict);

							if (potentialConflict.NewsReelText != null) {
								NewsReelText += "BREAKING NEWS: " + (potentialConflict.NewsReelText + "  ");
							}
						}
					}
					break;

				default:
					break;
			}
		}
	}

	public void ResolveAll (CharacterObject[] saved) {
		string[] savedOccupations = new string[saved.Length];

		for (int c = 0; c < saved.Length; c++) {
			if (saved[c] != null)
				savedOccupations[c] = saved[c].Occupation;

			else
				break;
		}

		Conflict givenConflict;		
		for (int c = 0; c < m_CurrentConflicts.Count; c++) {
			givenConflict = m_CurrentConflicts[c] as Conflict; 
			
			// Try to resolve this Conflict with what we have
			if (givenConflict.ResolvesConflict (savedOccupations)) {
				Debug.Log ("Solved conflict (" + givenConflict.Name + "').");
				givenConflict.Solved = true;
				m_ResolvedConflicts.Add (givenConflict);
				m_CurrentConflicts.RemoveAt (c);
			}

			// Make sure we haven't solved this Conflict already with a different recipe
			else {
				foreach (Conflict solvedConflict in m_ResolvedConflicts) {
					if (givenConflict.Name == solvedConflict.Name) {
					givenConflict.Solved = true;
					Debug.Log ("Conflict was already been resolved (" + givenConflict.Name + "').");					}
				}		
			}
		}
	}

	public string GetRelevantResolutionText () {
		foreach (Conflict aConflict in m_CurrentConflicts) {
			if (!aConflict.Solved)
				return aConflict.Name;
		}

		return "Incredibly there were no consequences for your choices this time";
	}

	public CharacterObject[][] GetTracks () {
		CharacterObject[][] toBeReturned = new CharacterObject[2][];
		toBeReturned[0] = TrackOneVictims;
		toBeReturned[1] = TrackTwoVictims;
		return toBeReturned;
	}

	public void Reset () {
		m_CurrentConflicts = new ArrayList ();
		m_ResolvedConflicts = new ArrayList ();
		NewsReelText = "";
	}
}